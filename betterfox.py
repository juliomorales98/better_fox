#export list into csv file
def list_to_csv(_file_name, _data):
    import csv
    with open(_file_name,"w+") as file:
        writer = csv.writer(file)
        for i in _data:
            tmpItem = []
            try:
                for j in i:
                    tmpItem.append(j)
            except:
                tmpItem = []
                tmpItem.append(i)

            writer.writerows([tmpItem])
